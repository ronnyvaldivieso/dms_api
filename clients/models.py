from django.db import models

class Client(models.Model):
	first_name = models.CharField(max_length=20)
	last_name = models.CharField(max_length=20)
	phone_number = models.CharField(max_length=13, default="0")
	status = models.CharField(max_length=2, default="A")

	def __str__(self):
		return self.first_name + " " + self.last_name

class ClientAddress(models.Model):
	client = models.ForeignKey(Client, related_name='addresses', on_delete=models.CASCADE)
	main_street = models.CharField(max_length=50)
	second_street = models.CharField(max_length=50)
	house_number = models.CharField(max_length=5)

	def __str__(self):
		return '%s #%s y %s' % (self.main_street, self.house_number, self.second_street)