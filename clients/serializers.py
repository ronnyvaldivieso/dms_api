from rest_framework import serializers
from .models import Client, ClientAddress

class ClientAddressSerializer(serializers.ModelSerializer):
	class Meta:
		model = ClientAddress
		fields = ['main_street', 'second_street', 'house_number']

class ClientSerializer(serializers.ModelSerializer):
	# addresses = ClientAddressSerializer(many=True) 			# devuelve las direcciones en objetos con todos sus campos
	addresses = serializers.StringRelatedField(many=True)	# devuelve las direcciones en una sola cadena

	class Meta:
		model = Client
		fields = ['id', 'first_name', 'last_name', 'phone_number', 'status', 'addresses']
	
	def create(self, validated_data):
		addresses_data = validated_data.pop('addresses')
		client = Client.objects.create(**validated_data)
		for address_data in addresses_data:
				ClientAddress.objects.create(client=client, **address_data)
		return client