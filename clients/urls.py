from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [
	path('clients/', views.ClientView.as_view()),
	path('clientAddresses/', views.ClientAddressView.as_view())
]