from rest_framework import viewsets
from clients.serializers import ClientSerializer, ClientAddressSerializer
from .models import Client, ClientAddress
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import ClientSerializer, ClientAddressSerializer

class ClientView(APIView):
	def get(self, request):
		clients = Client.objects.all()
		serializer = ClientSerializer(clients, many=True)

		return Response(serializer.data)

	def post(self, request):
		print("REQUEST")
		print(request.data)
		client = ClientSerializer(data=request.data)
		
		if client.is_valid():
			print(client.data)
			client.save()
			return Response(client.data)

		return Response({"data": {}})

class ClientAddressView(APIView):
	def get(self, request):
		addresses = ClientAddress.objects.all()
		serializer = ClientAddressSerializer(addresses, many=True)
		return Response({"data": serializer.data})